package com.bigfans.framework.es.schema;

import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.settings.Settings.Builder;

public class IndexSettingsBuilder {

	private Builder settingsBuilder;

	public IndexSettingsBuilder() {
		settingsBuilder = Settings.builder();
	}
	
	public void putSettings(ElasticSchema schema){
		schema.build(this.settingsBuilder);
	}
	
	public void setNumberOfShards(int num) {
		settingsBuilder.put("index.number_of_shards", num);
	}
	
	public void setNumberOfReplicas(int num){
		settingsBuilder.put("index.number_of_replicas", num);
	}
	
	public void setCompress(boolean isTrue){
		settingsBuilder.put("index.compress", isTrue);
	}
	
	public void setCompressStored(boolean isTrue){
		settingsBuilder.put("index.store.compress.stored", isTrue);
	}
	
	public void setCompressTV(boolean isTrue){
		settingsBuilder.put("index.store.compress.tv", isTrue);
	}
	
	public void setRefreshInterval(String seconds){
		settingsBuilder.put("index.refresh_interval", seconds);
	}

	public void setTranslogFlushThresholdSize(String val){
		settingsBuilder.put("index.translog.flush_threshold_size", val);
	}
	
	public Settings getSettings() {
		return settingsBuilder.build();
	}

}
