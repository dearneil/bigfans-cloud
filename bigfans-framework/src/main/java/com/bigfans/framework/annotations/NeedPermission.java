package com.bigfans.framework.annotations;

/**
 * @author lichong
 * @create 2018-05-02 下午10:15
 **/
@NeedLogin
public @interface NeedPermission {

    String[] permissions() default {};

}
