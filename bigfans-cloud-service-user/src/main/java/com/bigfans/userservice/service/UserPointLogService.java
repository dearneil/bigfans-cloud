package com.bigfans.userservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.userservice.model.UserPointLog;

public interface UserPointLogService extends BaseService<UserPointLog>{
}
