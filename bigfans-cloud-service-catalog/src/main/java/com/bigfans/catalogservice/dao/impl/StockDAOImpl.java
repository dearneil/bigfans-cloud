package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.StockDAO;
import com.bigfans.catalogservice.model.Stock;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Stock log
 * 
 * @Description:
 * @author lichong
 * @date Mar 7, 2016 4:04:31 PM
 *
 */
@Repository(StockDAOImpl.BEAN_NAME)
public class StockDAOImpl extends MybatisDAOImpl<Stock> implements StockDAO {

	public static final String BEAN_NAME = "stockDAO";


	@Override
	public Integer stockOut(String prodId, Integer amount) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		params.put("amount", amount);
		return getSqlSession().update(className +".stockOut", params);
	}

	@Override
	public Integer stockIn(String prodId, Integer amount) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		params.put("amount", amount);
		return getSqlSession().update(className +".stockIn", params);
	}

	@Override
	public Integer lockStock(String prodId, Integer amount) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		params.put("amount", amount);
		return getSqlSession().update(className +".lockStock", params);
	}

	@Override
	public Integer releaseStock(String prodId, Integer amount) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		params.put("amount", amount);
		return getSqlSession().update(className +".releaseStock", params);
	}

	@Override
	public Stock getByProd(String prodId) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		return getSqlSession().selectOne(className +".load", params);
	}

	@Override
	public Stock getBySkuValKey(String skuValKey) {
		ParameterMap params = new ParameterMap();
		params.put("skuValKey", skuValKey);
		return getSqlSession().selectOne(className +".load", params);
	}

}
