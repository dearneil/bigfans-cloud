package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.ProductGroupAttributeDAO;
import com.bigfans.catalogservice.model.ProductGroupAttribute;
import com.bigfans.framework.dao.BeanDecorator;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 商品和属性值关联表DAO
 * @author lichong 
 * @date 2015年12月21日 下午9:55:11 
 * @version V1.0
 */
@Repository(ProductGroupAttributeDAOImpl.BEAN_NAME)
public class ProductGroupAttributeDAOImpl extends MybatisDAOImpl<ProductGroupAttribute> implements ProductGroupAttributeDAO {

	public static final String BEAN_NAME = "productGroupAttributeDAO";
	
	@Override
	public List<ProductGroupAttribute> listByPgId(String pgId) {
		ParameterMap params = new ParameterMap();
		params.put("pgId", pgId);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public int batchInsert(List<ProductGroupAttribute> objList) {
		return new BeanDecorator(objList).batchInsert();
	}
}
