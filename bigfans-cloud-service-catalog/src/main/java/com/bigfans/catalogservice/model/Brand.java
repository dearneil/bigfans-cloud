package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.BrandEntity;
import lombok.Data;

import java.util.List;

@Data
public class Brand extends BrandEntity {

	private static final long serialVersionUID = 2791272843557322232L;

	private List<String> categories;

}
