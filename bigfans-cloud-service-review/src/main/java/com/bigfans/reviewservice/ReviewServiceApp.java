package com.bigfans.reviewservice;

import com.bigfans.Constants;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.CurrentUserFactory;
import com.bigfans.reviewservice.api.auth.ReviewServiceFunctionalUser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lichong
 * @create 2018-02-14 下午8:05
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class ReviewServiceApp {

    public static void main(String[] args) throws Exception{
        CurrentUser functionalUser = new ReviewServiceFunctionalUser();
        String functionalUserToken = CurrentUserFactory.createToken(functionalUser , Constants.TOKEN.JWT_SECURITY_KEY);
        ReviewApplications.setFunctionalUser(functionalUser);
        ReviewApplications.setFunctionalUserToken(functionalUserToken);
        SpringApplication.run(ReviewServiceApp.class , args);
    }

}
