package com.bigfans.orderservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.orderservice.dao.InvoiceDAO;
import com.bigfans.orderservice.model.Invoice;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author lichong
 *
 */
@Repository(InvoiceDAOImpl.BEAN_NAME)
public class InvoiceDAOImpl extends MybatisDAOImpl<Invoice> implements InvoiceDAO {

	public static final String BEAN_NAME = "invoiceDAO";

}
