package com.bigfans.orderservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:配送方式
 * @author lichong 
 * 2015年4月2日上午11:17:59
 *
 */
@Data
@Table(name="DeliveryMethod")
public class DeliveryMethodEntity extends AbstractModel {

	private static final long serialVersionUID = 6855285633146917004L;

	@Column(name="name")
	protected String name;
	@Column(name="details")
	protected String details;
	@Column(name="tips")
	protected Float tips;
	@Column(name="order_num")
	protected Integer orderNum;
	
	public String getModule() {
		return "DeliveryMethod";
	}

}
