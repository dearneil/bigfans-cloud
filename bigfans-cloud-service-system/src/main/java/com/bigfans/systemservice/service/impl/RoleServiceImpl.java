package com.bigfans.systemservice.service.impl;

import com.bigfans.systemservice.model.Role;
import com.bigfans.systemservice.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @author lichong
 * @create 2018-04-19 下午9:20
 **/
@Service(RoleServiceImpl.BEAN_NAME)
public class RoleServiceImpl implements RoleService {

    public static final String BEAN_NAME = "roleServiceImpl";

    @Override
    @Transactional(readOnly = true)
    public List<Role> listRolesByUser(String userId) throws Exception {
        // TODO mock
        Role operator = new Role();
        operator.setName("operator");
        return Arrays.asList(operator);
    }
}
