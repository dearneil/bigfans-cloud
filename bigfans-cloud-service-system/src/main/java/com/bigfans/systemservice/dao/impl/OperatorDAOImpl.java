package com.bigfans.systemservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.systemservice.dao.OperatorDAO;
import com.bigfans.systemservice.model.Operator;
import org.springframework.stereotype.Repository;

@Repository(OperatorDAOImpl.BEAN_NAME)
public class OperatorDAOImpl extends MybatisDAOImpl<Operator> implements OperatorDAO {

	public static final String BEAN_NAME = "operatorDAO";
	
	public int countByUsername(String account){
		ParameterMap params = new ParameterMap();
		params.put("account", account);
		return getSqlSession().update(className + ".count", params);
	}

}